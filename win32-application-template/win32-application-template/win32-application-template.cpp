#include <windows.h>

class Application
{
public:
	HDC			hDC;
	HGLRC		hRC;
	HWND		hWnd;
	HINSTANCE	hInstance;
	int			borderWidth;
	int			borderHeight;

	DEVMODE		nativeMode;

	bool createWindow(LPCWSTR title, WNDPROC WndProc,
		long width, long height);
};

bool Application::createWindow(LPCWSTR title, WNDPROC WndProc,
	long width, long height)
{
	WNDCLASS	wc;
	DWORD		dwExStyle;
	DWORD		dwStyle;
	RECT		rc;

	EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &nativeMode);

	long x = nativeMode.dmPelsWidth / 2 - width / 2;
	long y = nativeMode.dmPelsHeight / 2 - height / 2;

	rc.left = (long)x;
	rc.right = (long)x + width;
	rc.top = (long)y;
	rc.bottom = (long)y + height;

	hInstance = GetModuleHandle(NULL);
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = (WNDPROC)WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = NULL;
	wc.lpszMenuName = NULL;
	wc.lpszClassName = __TEXT("OGL");

	RegisterClass(&wc);

	dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;
	dwStyle = WS_OVERLAPPEDWINDOW | WS_MINIMIZEBOX | WS_CAPTION | WS_SYSMENU
		| WS_CLIPSIBLINGS | WS_CLIPCHILDREN;

	AdjustWindowRectEx(&rc, dwStyle, false, dwExStyle);

	hWnd = CreateWindowEx(dwExStyle, __TEXT("OGL"), title, dwStyle,
		rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top,
		NULL, NULL, hInstance, NULL);

	RECT rcClient, rcWind;
	GetClientRect(hWnd, &rcClient);
	GetWindowRect(hWnd, &rcWind);
	borderWidth = 2 * ((rcWind.right - rcWind.left) - rcClient.right) / 2;
	borderHeight = 2 * ((rcWind.bottom - rcWind.top) - rcClient.bottom) / 2;

	SetForegroundWindow(hWnd);
	SetFocus(hWnd);
	ShowWindow(hWnd, SW_SHOW);
	return true;
}

Application app;

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{

	case WM_GETMINMAXINFO:
		((MINMAXINFO *)lParam)->ptMinTrackSize.x = 320 + app.borderWidth;
		((MINMAXINFO *)lParam)->ptMinTrackSize.y = 240 + app.borderHeight;
		return 0;

	case WM_CLOSE:
		PostQuitMessage(0);
		return 0;

	default:
		return DefWindowProc(hWnd, message, wParam, lParam);

	}
}

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	MSG msg;
	bool quit = false;

	app.createWindow(__TEXT("window"), WndProc, 320, 240);
	
	while (!quit)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				quit = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			//runloop
		}
	}

	//cleanup
	return msg.wParam;
}